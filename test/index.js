var assert   = require("assert");
var Bluebird = require("bluebird");
var timing   = require("../");

describe("simplestats-timing", function() {
	var headers;
  var loggerObj;
	var res = {
		setHeader: function(k, v) {
			headers[k] = v;
		}
	};

  function logger(obj) {
    loggerObj = obj;
  }

	beforeEach(function() {
		headers = {};
		res.timing = timing(res, logger);
	});

	// Basic first test...
	it("should accept promise", function(done) {
		var called;

		function promiseFn() {
			called = true;
			return new Bluebird(function(resolve) {
				resolve();
			});
		}

		res.timing("sql:auth", promiseFn)
			.then(function() {
				process.nextTick(function() {
					assert(called);
					assert(headers)
					assert(headers["x-simplestats-timing"])

          var obj = JSON.parse(headers["x-simplestats-timing"]);
					assert.equal(obj["sql:auth"].metric, "ms")
					assert(obj["sql:auth"].value)

          assert(loggerObj, {
            "simplestats-timing": {
              "sql:auth": {
                "metric": "ms",
                "value": obj["sql:auth"].metric
              }
            }
          });

					done();
				});
			});
	})

	it("should accept callback", function(done) {
		var called;

		function cb(done) {
			called = true;
			done();
		}

		res.timing("sql:auth", cb)

		assert(called);
		assert(headers)
		assert(headers["x-simplestats-timing"])

    var obj = JSON.parse(headers["x-simplestats-timing"]);
    assert.equal(obj["sql:auth"].metric, "ms")
    assert(obj["sql:auth"].value)

    assert(loggerObj, {
      "simplestats-timing": {
        "sql:auth": {
          "metric": "ms",
          "value": obj["sql:auth"].metric
        }
      }
    });

		done();
	})
});
