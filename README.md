# simplestats-timing
Time some things and add in [simplestats]() headers to your response headers


## API
Just pass it an express reponse object to initialize

    var timing = require("simplestats-timing");
    res.timing = timing(res, logger.info.bind(logger));

Via a callback
 
    res.timing("sql:auth_query", function(done) {
      setTimeout(done, 100); 
    });

Via a promise

    res.timing("sql:auth_query", function() {
      return Promise.resolve();
    });

Or inline

    var timer = res.timing("sql:auth_query");
    doSomethingUseful();
    timer.end();


# License
MIT
