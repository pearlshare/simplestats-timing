var now = require("performance-now");

function addHeader(res, key, took) {
  // Start a cache on the res
  var obj = res.simplestats = res.simplestats || {};
  obj[key] = {
    metric: "ms",
    value: took
  };

  // Always set the headers
  res.setHeader("x-simplestats-timing", JSON.stringify(obj));
}


module.exports = function(res, logger) {
  // TODO: opts
  return function(key, fn, newThis) {
    var s = now();
    var ended = false;

    function end() {
      if(ended) {
        return;
      }

      var took = now()-s;
      ended = true;

      addHeader(res, key, took);

      // Log (if defined)
      if(logger) {
        var obj = {};
        obj[key] = {
          metric: "ms",
          value: took
        };

        logger({
          "simplestats-timing": obj
        });
      }
    }

    if(fn) {
      var ret = fn.call(newThis, end);
      if(ret && ret.then) {
        ret.then(end);
      }
      return ret;
    } else {
      return {
        end: end
      }
    }
  };
};
